CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "User" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT UNIQUE NOT NULL,
  "email" TEXT UNIQUE NOT NULL,
  "password" TEXT NOT NULL,
  "admin" BOOLEAN NOT NULL DEFAULT FALSE,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "UserToken" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "userId" uuid NOT NULL,
  FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE
);

CREATE TABLE "TagCategory" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT UNIQUE NOT NULL,
  "default" BOOLEAN NOT NULL DEFAULT FALSE,
  "color" TEXT NOT NULL DEFAULT '#000000',
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "Tag" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT UNIQUE NOT NULL,
  "description" TEXT NOT NULL,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "categoryId" uuid NOT NULL,
  FOREIGN KEY ("categoryId") REFERENCES "TagCategory"("id") ON DELETE CASCADE
);

CREATE TABLE "TagAlias" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT UNIQUE NOT NULL,
  "tagId" uuid NOT NULL,
  FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE CASCADE
);

CREATE TABLE "_TagImplication" (
  "A" uuid NOT NULL,
  FOREIGN KEY ("A") REFERENCES "Tag"("id") ON DELETE CASCADE,
  "B" uuid NOT NULL,
  FOREIGN KEY ("B") REFERENCES "Tag"("id") ON DELETE CASCADE
);

CREATE UNIQUE INDEX "_TagImplication_AB_unique" ON "_TagImplication"("A", "B");
CREATE INDEX "_TagImplication_B_index" ON "_TagImplication"("B");

CREATE TABLE "_TagSuggestion" (
  "A" uuid NOT NULL,
  FOREIGN KEY ("A") REFERENCES "Tag"("id") ON DELETE CASCADE,
  "B" uuid NOT NULL,
  FOREIGN KEY ("B") REFERENCES "Tag"("id") ON DELETE CASCADE
);

CREATE UNIQUE INDEX "_TagSuggestion_AB_unique" ON "_TagSuggestion"("A", "B");
CREATE INDEX "_TagSuggestion_B_index" ON "_TagSuggestion"("B");

CREATE TABLE "GalleryCategory" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT UNIQUE NOT NULL,
  "default" BOOLEAN NOT NULL DEFAULT FALSE,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "Gallery" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "name" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "source" TEXT,
  "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "authorId" uuid NOT NULL,
  FOREIGN KEY ("authorId") REFERENCES "User"("id") ON DELETE CASCADE,
  "categoryId" uuid NOT NULL,
  FOREIGN KEY ("categoryId") REFERENCES "GalleryCategory"("id") ON DELETE CASCADE
);

CREATE TABLE "_GalleryTag" (
  "A" uuid NOT NULL,
  FOREIGN KEY ("A") REFERENCES "Gallery"("id") ON DELETE CASCADE,
  "B" uuid NOT NULL,
  FOREIGN KEY ("B") REFERENCES "Tag"("id") ON DELETE CASCADE
);

CREATE UNIQUE INDEX "_GalleryTag_AB_unique" ON "_GalleryTag"("A", "B");
CREATE INDEX "_GalleryTag_B_index" ON "_GalleryTag"("B");

CREATE TABLE "Image" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "position" INTEGER NOT NULL,
  "galleryId" uuid NOT NULL,
  FOREIGN KEY ("galleryId") REFERENCES "Gallery"("id") ON DELETE CASCADE,
  UNIQUE ("galleryId", "position")
);

INSERT INTO "TagCategory" ("name", "default") VALUES ('default', TRUE);
INSERT INTO "GalleryCategory" ("name", "default") VALUES ('default', TRUE);
