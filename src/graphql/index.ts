import {
  asNexusMethod,
  connectionPlugin,
  fieldAuthorizePlugin,
  makeSchema,
  queryComplexityPlugin
} from "@nexus/schema";
import { GraphQLDate, GraphQLDateTime, GraphQLTime } from "graphql-iso-date";
import { ForbiddenError } from "apollo-server-core";
import { GraphQLUpload } from "graphql-upload";
import { GraphQLScalarType } from "graphql";

import { UserTypes } from "./user";
import { UserTokenTypes } from "./userToken";
import { GalleryCategoryTypes } from "./galleryCategory";
import { TagCategoryTypes } from "./tagCategory";
import { TagTypes } from "./tag";
import { TagAliasTypes } from "./tagAlias";
import { GalleryTypes } from "./gallery";
import { ImageTypes } from "./image";
import { SupportTypes } from "./support";

export const schema = makeSchema({
  shouldGenerateArtifacts: false,
  outputs: false,
  nonNullDefaults: {
    output: true,
    input: true
  },
  types: [
    asNexusMethod(GraphQLDate as GraphQLScalarType, "date"),
    asNexusMethod(GraphQLTime as GraphQLScalarType, "time"),
    asNexusMethod(GraphQLDateTime as GraphQLScalarType, "dateTime"),
    asNexusMethod(GraphQLUpload as GraphQLScalarType, "upload"),
    ...UserTypes,
    ...UserTokenTypes,
    ...GalleryCategoryTypes,
    ...TagCategoryTypes,
    ...TagTypes,
    ...TagAliasTypes,
    ...GalleryTypes,
    ...ImageTypes,
    ...SupportTypes
  ],
  plugins: [
    connectionPlugin(),
    queryComplexityPlugin(),
    fieldAuthorizePlugin({
      formatError: () => new ForbiddenError("Not authorized")
    })
  ]
});
