import { models } from "./model";
import { queries } from "./query";
import { mutations } from "./mutation";

export const TagCategoryTypes = [...models, ...queries, ...mutations];

export { TagCategoryWhereArg, TagCategoryOrderByArg } from "./model";
