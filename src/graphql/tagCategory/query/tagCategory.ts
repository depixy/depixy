import { queryIdField } from "../../func";

const tagCategory = queryIdField("tagCategory", {
  type: "TagCategory",
  resolve: (id, ctx) => ctx.prisma.tagCategory.findOne({ where: { id } })
});
export const tagCategoryTypes = [tagCategory];
