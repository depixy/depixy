import { tagCategoryTypes } from "./tagCategory";
import { tagCategoriesTypes } from "./tagCategories";

export const queries = [...tagCategoriesTypes, ...tagCategoryTypes];
