import { objectType, queryField } from "@nexus/schema";
import { TagCategory } from "@prisma/client";

import { createSelectArg, Pagination, SelectArg } from "../../support";
import { AppContext } from "../../type";
import { TagCategoryOrderByArg, TagCategoryWhereArg } from "../model";

type Args = SelectArg<TagCategoryWhereArg, TagCategoryOrderByArg>;

const tagCategoriesType = objectType({
  name: "TagCategories",
  definition(t) {
    t.list.field("data", {
      type: "TagCategory",
      args: createSelectArg({
        whereType: "TagCategoryWhere",
        orderByType: "TagCategoryOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<TagCategory[]> => ctx.prisma.tagCategory.findMany(args)
    });
    t.field("pagination", {
      type: "Pagination",
      args: createSelectArg({
        whereType: "TagCategoryWhere",
        orderByType: "TagCategoryOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<Pagination> => {
        const { where, take, skip } = args;
        const total = await ctx.prisma.tagCategory.count({ where });
        return { take, skip, total };
      }
    });
  }
});

const tagCategories = queryField("tagCategories", {
  type: "TagCategories",
  args: createSelectArg({
    whereType: "TagCategoryWhere",
    orderByType: "TagCategoryOrderBy"
  }),
  resolve: async (root, args: Args, ctx: AppContext) => ({})
});

export const tagCategoriesTypes = [tagCategories, tagCategoriesType];
