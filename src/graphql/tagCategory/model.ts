import { inputObjectType, objectType } from "@nexus/schema";

import { DataTimeWhereArg, OrderByArg, StringWhereArg } from "../support";

const tagCategory = objectType({
  name: "TagCategory",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.string("color");
    t.boolean("default");
    t.dateTime("createdAt");
  }
});

const tagCategoryOrderBy = inputObjectType({
  name: "TagCategoryOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const tagCategoryWhere = inputObjectType({
  name: "TagCategoryWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.list.field("AND", { type: "TagCategoryWhere", nullable: true });
    t.list.field("OR", { type: "TagCategoryWhere", nullable: true });
    t.list.field("NOT", { type: "TagCategoryWhere", nullable: true });
  }
});

export const models = [tagCategory, tagCategoryOrderBy, tagCategoryWhere];

export interface TagCategoryOrderByArg {
  name?: OrderByArg;
  createdAt?: OrderByArg;
}

export interface TagCategoryWhereArg {
  name?: StringWhereArg;
  createdAt?: DataTimeWhereArg;
  AND?: TagCategoryWhereArg[];
  OR?: TagCategoryWhereArg[];
  NOT?: TagCategoryWhereArg[];
}
