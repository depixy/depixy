import { deleteById } from "../../func";
import { AppContext } from "../../type";

const removeTagCategory = deleteById({
  fieldName: "removeTagCategory",
  type: "TagCategory",
  resolve: async (id, ctx: AppContext) => {
    await ctx.hasPermission("tagCategory::remove");
    return ctx.prisma.tagCategory.delete({
      where: { id }
    });
  }
});

export const removeTagCategoryTypes = [removeTagCategory];
