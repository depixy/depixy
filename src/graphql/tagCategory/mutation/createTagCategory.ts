import { mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  name: string;
  color: string;
};
const createTagCategory = mutationField("createTagCategory", {
  type: "TagCategory",
  args: {
    name: stringArg(),
    color: stringArg()
  },
  resolve: async (_, args: Args, ctx: AppContext) => {
    const { name, color } = args;
    await ctx.hasPermission("tagCategory::create");
    return ctx.prisma.tagCategory.create({
      data: {
        name,
        color
      }
    });
  }
});

export const createTagCategoryTypes = [createTagCategory];
