import { createTagCategoryTypes } from "./createTagCategory";
import { removeTagCategoriesTypes } from "./removeTagCategories";
import { removeTagCategoryTypes } from "./removeTagCategory";
import { updateTagCategoryTypes } from "./updateTagCategory";

export const mutations = [
  createTagCategoryTypes,
  removeTagCategoriesTypes,
  removeTagCategoryTypes,
  updateTagCategoryTypes
];
