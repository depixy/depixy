import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeTagCategories = deleteByIds({
  fieldName: "removeTagCategories",
  resolve: async (ids, ctx: AppContext) => {
    await ctx.hasPermission("tagCategory::remove");
    return ctx.prisma.tagCategory.deleteMany({
      where: {
        id: { in: ids }
      }
    });
  }
});

export const removeTagCategoriesTypes = [removeTagCategories];
