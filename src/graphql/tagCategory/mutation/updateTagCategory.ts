import { idArg, mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  id: string;
  name?: string;
  color?: string;
};

const updateTagCategory = mutationField("updateTagCategory", {
  type: "TagCategory",
  args: {
    id: idArg(),
    name: stringArg({ nullable: true }),
    color: stringArg({ nullable: true })
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const { id, name, color } = args;
    await ctx.hasPermission("tagCategory::edit");
    return ctx.prisma.tagCategory.update({
      data: { name, color },
      where: { id }
    });
  }
});

export const updateTagCategoryTypes = [updateTagCategory];
