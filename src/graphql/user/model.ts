import { inputObjectType, objectType } from "@nexus/schema";
import { User } from "@prisma/client";

import { AppContext } from "../type";
import {
  BooleanWhereArg,
  DataTimeWhereArg,
  OrderByArg,
  StringWhereArg
} from "../support";

const user = objectType({
  name: "User",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.boolean("admin");
    t.dateTime("createdAt");
  }
});

const userDetail = objectType({
  name: "UserDetail",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.boolean("admin");
    t.string("email");
    t.dateTime("createdAt");
    t.list.field("userTokens", {
      type: "UserToken",
      resolve: (root: User, _, ctx: AppContext) =>
        ctx.prisma.userToken.findMany({
          where: { userId: root.id },
          orderBy: { createdAt: "desc" }
        })
    });
  }
});

const userOrderBy = inputObjectType({
  name: "UserOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const userWhere = inputObjectType({
  name: "UserWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.field("admin", { type: "BooleanWhere", nullable: true });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.list.field("AND", { type: "UserWhere", nullable: true });
    t.list.field("OR", { type: "UserWhere", nullable: true });
    t.list.field("NOT", { type: "UserWhere", nullable: true });
  }
});

export const models = [user, userDetail, userOrderBy, userWhere];

export interface UserOrderByArg {
  name?: OrderByArg;
  createdAt?: OrderByArg;
}

export interface UserWhereArg {
  name?: StringWhereArg;
  createdAt?: DataTimeWhereArg;
  admin?: BooleanWhereArg;
  AND?: UserWhereArg[];
  OR?: UserWhereArg[];
  NOT?: UserWhereArg[];
}
