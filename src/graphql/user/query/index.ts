import { userTypes } from "./user";
import { usersTypes } from "./users";
import { userSelfTypes } from "./userSelf";

export const queries = [...usersTypes, ...userTypes, ...userSelfTypes];
