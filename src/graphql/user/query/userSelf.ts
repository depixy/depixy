import { queryField } from "@nexus/schema";

import { AppContext } from "../../type";

const userSelf = queryField("userSelf", {
  type: "UserDetail",
  nullable: true,
  resolve: async (root, arg, ctx: AppContext) => {
    const user = await ctx.getUserOrThrow();
    return ctx.prisma.user.findOne({
      where: { id: user.id }
    });
  }
});

export const userSelfTypes = [userSelf];
