import { queryIdField } from "../../func";

const user = queryIdField("user", {
  type: "User",
  resolve: (id, ctx) => ctx.prisma.user.findOne({ where: { id } })
});
export const userTypes = [user];
