import { objectType, queryField } from "@nexus/schema";
import { User } from "@prisma/client";

import { createSelectArg, Pagination, SelectArg } from "../../support";
import { AppContext } from "../../type";
import { UserOrderByArg, UserWhereArg } from "../model";

type Args = SelectArg<UserWhereArg, UserOrderByArg>;

const usersType = objectType({
  name: "Users",
  definition(t) {
    t.list.field("data", {
      type: "User",
      args: createSelectArg({
        whereType: "UserWhere",
        orderByType: "UserOrderBy"
      }),
      resolve: async (root, args: Args, ctx: AppContext): Promise<User[]> =>
        ctx.prisma.user.findMany(args)
    });
    t.field("pagination", {
      type: "Pagination",
      args: createSelectArg({
        whereType: "UserWhere",
        orderByType: "UserOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<Pagination> => {
        const { where, take, skip } = args;
        const total = await ctx.prisma.user.count({ where });
        return { take, skip, total };
      }
    });
  }
});

const users = queryField("users", {
  type: "Users",
  args: createSelectArg({
    whereType: "UserWhere",
    orderByType: "UserOrderBy"
  }),
  resolve: async (root, args: Args, ctx: AppContext) => ({})
});

export const usersTypes = [users, usersType];
