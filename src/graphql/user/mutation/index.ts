import { createUserTypes } from "./createUser";
import { removeUsersTypes } from "./removeUsers";
import { removeUserTypes } from "./removeUser";
import { updateUserTypes } from "./updateUser";

export const mutations = [
  createUserTypes,
  removeUsersTypes,
  removeUserTypes,
  updateUserTypes
];
