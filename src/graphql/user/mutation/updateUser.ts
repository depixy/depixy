import { idArg, mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  id: string;
  email?: string;
  password?: string;
};

const updateUser = mutationField("updateUser", {
  type: "User",
  args: {
    id: idArg(),
    name: stringArg({ nullable: true }),
    email: stringArg(),
    password: stringArg()
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const { id, email, password: rawPassword } = args;
    await ctx.hasPermission("user::edit");
    const password = rawPassword
      ? await ctx.hashPassword(rawPassword)
      : rawPassword;
    return ctx.prisma.user.update({
      data: { email, password },
      where: { id }
    });
  }
});

export const updateUserTypes = [updateUser];
