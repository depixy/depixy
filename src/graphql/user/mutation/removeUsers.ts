import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeUsers = deleteByIds({
  fieldName: "removeUsers",
  resolve: async (ids, ctx: AppContext) => {
    await ctx.hasPermission("user::remove");
    return ctx.prisma.galleryCategory.deleteMany({
      where: {
        id: { in: ids }
      }
    });
  }
});

export const removeUsersTypes = [removeUsers];
