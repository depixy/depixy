import { deleteById } from "../../func";
import { AppContext } from "../../type";

const removeUser = deleteById({
  fieldName: "removeUser",
  type: "User",
  resolve: async (id, ctx: AppContext) => {
    await ctx.hasPermission("user::remove");
    return ctx.prisma.user.delete({
      where: { id }
    });
  }
});

export const removeUserTypes = [removeUser];
