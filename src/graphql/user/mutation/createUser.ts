import { mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  name: string;
  email: string;
  password: string;
};
const createUser = mutationField("createUser", {
  type: "User",
  args: {
    name: stringArg(),
    email: stringArg(),
    password: stringArg()
  },
  resolve: async (_, args: Args, ctx: AppContext) => {
    const { name, email, password: rawPassword } = args;
    const password = await ctx.hashPassword(rawPassword);
    const userCount = await ctx.prisma.user.count();
    const isUserExists = userCount > 0;
    if (isUserExists) {
      await ctx.hasPermission("user::create");
    }
    return ctx.prisma.user.create({
      data: { name, email, password, admin: !isUserExists }
    });
  }
});

export const createUserTypes = [createUser];
