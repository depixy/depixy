import { models } from "./model";
import { queries } from "./query";
import { mutations } from "./mutation";

export const UserTypes = [...models, ...queries, ...mutations];

export { UserWhereArg, UserOrderByArg } from "./model";
