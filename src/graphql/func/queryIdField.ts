import { FieldResolver, idArg, queryField } from "@nexus/schema";

import { AppContext } from "../../middleware";
import { Query } from "../type";

interface Args {
  id: string;
}

interface Option {
  type: string;
  resolve: (
    id: string,
    ctx: AppContext
  ) => ReturnType<FieldResolver<"Query", string>>;
}

export const queryIdField = (fieldName: string, option: Option): Query => {
  const { type, resolve } = option;
  return queryField(fieldName, {
    type,
    args: {
      id: idArg()
    },
    nullable: true,
    resolve: (root, args: Args, ctx: AppContext) => {
      const { id } = args;
      return resolve(id, ctx);
    }
  });
};
