import { FieldResolver, idArg, mutationField } from "@nexus/schema";

import { Mutation } from "../type";
import { AppContext } from "../../middleware";

type RemoveArgs = {
  id: string;
};

type Option = {
  fieldName: string;
  type: string;
  resolve: (
    id: string,
    ctx: AppContext
  ) => ReturnType<FieldResolver<"Query", string>>;
};

export const deleteById = (option: Option): Mutation => {
  const { fieldName, type, resolve } = option;
  return mutationField(fieldName, {
    type,
    args: {
      id: idArg()
    },
    nullable: true,
    resolve: async (_, args: RemoveArgs, ctx: AppContext) => {
      const { id } = args;
      try {
        return resolve(id, ctx);
      } catch {
        return null;
      }
    }
  });
};

type RemoveListArgs = {
  ids: string[];
};

type ListOption = {
  fieldName: string;
  resolve: (
    ids: string[],
    ctx: AppContext
  ) => ReturnType<FieldResolver<"Query", string>>;
};

export const deleteByIds = (option: ListOption): Mutation => {
  const { fieldName, resolve } = option;
  return mutationField(fieldName, {
    type: "BatchResult",
    args: {
      ids: idArg({ list: true })
    },
    resolve: async (_, args: RemoveListArgs, ctx: AppContext) => {
      const { ids } = args;
      return resolve(ids, ctx);
    }
  });
};
