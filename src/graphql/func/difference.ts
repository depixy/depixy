import _ from "lodash";

export const difference = <T>(
  currentValues: T[],
  newValues: T[]
): [T[], T[]] => {
  const deleteValues = _.difference(currentValues, newValues);
  const createValues = _.difference(newValues, currentValues);
  return [createValues, deleteValues];
};
