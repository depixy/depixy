import { models } from "./model";
import { queries } from "./query";
import { mutations } from "./mutation";

export const TagTypes = [...models, ...queries, ...mutations];

export { TagWhereArg, TagOrderByArg, TagFilterArg } from "./model";
