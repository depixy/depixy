import { inputObjectType, objectType } from "@nexus/schema";
import { Tag } from "@prisma/client";

import { DataTimeWhereArg, OrderByArg, StringWhereArg } from "../support";
import { AppContext } from "../type";
import { TagCategoryWhereArg } from "../tagCategory";
import { TagAliasFilterArg } from "../tagAlias";

const tag = objectType({
  name: "Tag",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.string("description");
    t.dateTime("createdAt");
    t.field("category", {
      type: "TagCategory",
      resolve: (root: Tag, _, ctx: AppContext) =>
        ctx.prisma.tagCategory.findOne({ where: { id: root.categoryId } })
    });
    t.list.string("alias", {
      resolve: async (root: Tag, _, ctx: AppContext) =>
        ctx.prisma.tagAlias
          .findMany({
            where: { tagId: root.id },
            select: { name: true }
          })
          .then(result => result.map(({ name }) => name))
    });
    t.list.field("implication", {
      type: "Tag",
      resolve: async (root: Tag, _, ctx: AppContext) =>
        ctx.prisma.tag.findMany({
          where: {
            Tag_B_TagImplication: {
              some: {
                id: root.id
              }
            }
          }
        })
    });
    t.list.field("suggestion", {
      type: "Tag",
      resolve: async (root: Tag, _, ctx: AppContext) =>
        ctx.prisma.tag.findMany({
          where: {
            Tag_B_TagSuggestion: {
              some: {
                id: root.id
              }
            }
          }
        })
    });
  }
});

const tagOrderBy = inputObjectType({
  name: "TagOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const tagWhere = inputObjectType({
  name: "TagWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.field("TagCategory", { type: "TagCategoryWhere", nullable: true });
    t.field("TagAlias", { type: "TagAliasFilter", nullable: true });
    t.list.field("AND", { type: "TagWhere", nullable: true });
    t.list.field("OR", { type: "TagWhere", nullable: true });
    t.list.field("NOT", { type: "TagWhere", nullable: true });
  }
});

const tagFilter = inputObjectType({
  name: "TagFilter",
  definition(t) {
    t.field("every", { type: "TagWhere", nullable: true });
    t.field("some", { type: "TagWhere", nullable: true });
    t.field("none", { type: "TagWhere", nullable: true });
  }
});

export const models = [tag, tagOrderBy, tagWhere, tagFilter];

export interface TagOrderByArg {
  name?: OrderByArg;
  createdAt?: OrderByArg;
}

export interface TagWhereArg {
  name?: StringWhereArg;
  createdAt?: DataTimeWhereArg;
  TagCategory?: TagCategoryWhereArg;
  TagAlias?: TagAliasFilterArg;
  AND?: TagWhereArg[];
  OR?: TagWhereArg[];
  NOT?: TagWhereArg[];
}

export interface TagFilterArg {
  every?: TagWhereArg;
  some?: TagWhereArg;
  none?: TagWhereArg;
}
