import { idArg, mutationField, stringArg } from "@nexus/schema";
import { UserInputError } from "apollo-server-koa";

import { difference } from "../../func";
import { AppContext } from "../../type";

type Args = {
  id: string;
  name?: string;
  description?: string;
  categoryId?: string;
  aliases: string[];
  tagSuggestionIds: string[];
  tagImplicationIds: string[];
};

const updateTag = mutationField("updateTag", {
  type: "Tag",
  args: {
    id: idArg(),
    name: stringArg({ nullable: true }),
    description: stringArg({ nullable: true }),
    categoryId: stringArg({ nullable: true }),
    aliases: stringArg({ required: false, list: true }),
    tagSuggestionIds: stringArg({ required: false, list: true }),
    tagImplicationIds: stringArg({ required: false, list: true })
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const {
      id,
      name,
      description,
      categoryId,
      aliases,
      tagSuggestionIds,
      tagImplicationIds
    } = args;
    await ctx.hasPermission("tag::edit");
    const tag = await ctx.prisma.tag.findOne({
      where: { id },
      include: {
        TagAlias: true,
        Tag_B_TagImplication: true,
        Tag_B_TagSuggestion: true
      }
    });
    if (!tag) {
      throw new UserInputError("Id does not exists.");
    }
    const currentAlias = tag.TagAlias.map(alias => alias.name);
    const [createAlias, deleteAlias] = difference(currentAlias, aliases);
    const currentImplicationIds = tag.Tag_B_TagImplication.map(tag => tag.id);
    const [createImplicationIds, deleteImplicationIds] = difference(
      currentImplicationIds,
      tagImplicationIds
    );
    const currentSuggestionIds = tag.Tag_B_TagSuggestion.map(tag => tag.id);
    const [createSuggestionIds, deleteSuggestionIds] = difference(
      currentSuggestionIds,
      tagSuggestionIds
    );
    const category = categoryId ? { connect: { id: categoryId } } : undefined;
    return ctx.prisma.tag.update({
      data: {
        name,
        description,
        TagCategory: category,
        TagAlias: {
          create: createAlias.map(name => ({ name })),
          delete: deleteAlias.map(name => ({ name }))
        },
        Tag_B_TagImplication: {
          connect: createImplicationIds.map(id => ({ id })),
          disconnect: deleteImplicationIds.map(id => ({ id }))
        },
        Tag_B_TagSuggestion: {
          connect: createSuggestionIds.map(id => ({ id })),
          disconnect: deleteSuggestionIds.map(id => ({ id }))
        }
      },
      where: { id }
    });
  }
});

export const updateTagTypes = [updateTag];
