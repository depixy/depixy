import { createTagTypes } from "./createTag";
import { removeTagsTypes } from "./removeTags";
import { removeTagTypes } from "./removeTag";
import { updateTagTypes } from "./updateTag";

export const mutations = [
  createTagTypes,
  removeTagsTypes,
  removeTagTypes,
  updateTagTypes
];
