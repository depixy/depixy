import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeTags = deleteByIds({
  fieldName: "Tags",
  resolve: async (ids, ctx: AppContext) => {
    await await ctx.hasPermission("tag::remove");
    return ctx.prisma.tag.deleteMany({
      where: {
        id: { in: ids }
      }
    });
  }
});

export const removeTagsTypes = [removeTags];
