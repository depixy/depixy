import { mutationField, stringArg } from "@nexus/schema";
import { UserInputError } from "apollo-server-koa";

import { AppContext } from "../../type";

const getTagCategoryId = async (
  ctx: AppContext,
  categoryId?: string
): Promise<string | null> => {
  if (categoryId) {
    return categoryId;
  }
  const categories = await ctx.prisma.tagCategory.findMany({
    where: { default: true }
  });
  return categories.length === 1 ? categories[0].id : null;
};

type Args = {
  name: string;
  description: string;
  categoryId?: string;
  aliases: string[];
  tagSuggestionIds: string[];
  tagImplicationIds: string[];
};

const createTag = mutationField("createTag", {
  type: "Tag",
  args: {
    name: stringArg(),
    description: stringArg(),
    categoryId: stringArg({ nullable: true }),
    aliases: stringArg({ list: true }),
    tagSuggestionIds: stringArg({ list: true }),
    tagImplicationIds: stringArg({ list: true })
  },
  resolve: async (_, args: Args, ctx: AppContext) => {
    const {
      name,
      description,
      categoryId,
      aliases,
      tagImplicationIds,
      tagSuggestionIds
    } = args;
    await ctx.hasPermission("tag::create");

    const cid = await getTagCategoryId(ctx, categoryId);
    if (!cid) {
      throw new UserInputError("Cannot find category.");
    }

    return ctx.prisma.tag.create({
      data: {
        name,
        description,
        TagCategory: {
          connect: {
            id: cid
          }
        },
        TagAlias: {
          create: aliases.map(name => ({ name }))
        },
        Tag_B_TagSuggestion: {
          connect: tagSuggestionIds.map(id => ({ id }))
        },
        Tag_B_TagImplication: {
          connect: tagImplicationIds.map(id => ({ id }))
        }
      }
    });
  }
});

export const createTagTypes = [createTag];
