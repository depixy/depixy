import { deleteById } from "../../func";
import { AppContext } from "../../type";

const removeTag = deleteById({
  fieldName: "removeTag",
  type: "Tag",
  resolve: async (id, ctx: AppContext) => {
    await ctx.hasPermission("tag::remove");
    return ctx.prisma.tag.delete({
      where: { id }
    });
  }
});

export const removeTagTypes = [removeTag];
