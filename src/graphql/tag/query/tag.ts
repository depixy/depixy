import { queryIdField } from "../../func";

const tag = queryIdField("tag", {
  type: "Tag",
  resolve: (id, ctx) => ctx.prisma.tag.findOne({ where: { id } })
});

export const tagTypes = [tag];
