import { objectType, queryField } from "@nexus/schema";
import { Tag } from "@prisma/client";

import { createSelectArg, Pagination, SelectArg } from "../../support";
import { AppContext } from "../../type";
import { TagOrderByArg, TagWhereArg } from "../model";

type Args = SelectArg<TagWhereArg, TagOrderByArg>;

const tagsType = objectType({
  name: "Tags",
  definition(t) {
    t.list.field("data", {
      type: "Tag",
      args: createSelectArg({
        whereType: "TagWhere",
        orderByType: "TagOrderBy"
      }),
      resolve: async (root, args: Args, ctx: AppContext): Promise<Tag[]> =>
        ctx.prisma.tag.findMany(args)
    });
    t.field("pagination", {
      type: "Pagination",
      args: createSelectArg({
        whereType: "TagWhere",
        orderByType: "TagOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<Pagination> => {
        const { where, take, skip } = args;
        const total = await ctx.prisma.tag.count({ where });
        return { take, skip, total };
      }
    });
  }
});

const tags = queryField("tags", {
  type: "Tags",
  args: createSelectArg({
    whereType: "TagWhere",
    orderByType: "TagOrderBy"
  }),
  resolve: async (root, args: Args, ctx: AppContext) => ({})
});

export const tagsTypes = [tags, tagsType];
