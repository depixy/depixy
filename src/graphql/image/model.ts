import { inputObjectType, objectType } from "@nexus/schema";
import { Image } from "@prisma/client";

import { OrderByArg } from "../support";
import { AppContext } from "../../middleware";

const image = objectType({
  name: "Image",
  definition(t) {
    t.int("position");
    t.field("gallery", {
      type: "Gallery",
      resolve: (root: Image, _, ctx: AppContext) =>
        ctx.prisma.gallery.findOne({ where: { id: root.galleryId } })
    });

    t.field("format", {
      type: "ImageFormat",
      resolve: async (root: Image, _, ctx: AppContext) => {
        const { origin, thumbnail } = ctx.image.createKeys(root.id);
        const promises = await Promise.all([
          ctx.s3.presignedGetObject(ctx.bucket, origin.join("/"), 24 * 60),
          ctx.s3.presignedGetObject(ctx.bucket, thumbnail.join("/"), 24 * 60)
        ]);
        return {
          origin: promises[0],
          thumbnail: promises[1]
        };
      }
    });
  }
});

const imageFormat = objectType({
  name: "ImageFormat",
  definition(t) {
    t.string("origin");
    t.string("thumbnail");
  }
});

const imageOrderBy = inputObjectType({
  name: "ImageOrderBy",
  definition(t) {
    t.field("position", { type: "OrderBy", nullable: true });
  }
});

export const models = [image, imageOrderBy, imageFormat];

export interface ImageOrderByArg {
  position?: OrderByArg;
}
