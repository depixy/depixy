import { arg, core, intArg } from "@nexus/schema";

export interface SelectArg<WhereArg, OrderByArg> {
  where?: WhereArg;
  orderBy?: OrderByArg;
  take: number;
  skip: number;
}

interface Option {
  whereType: string;
  orderByType: string;
  takeDefault?: number;
  skipDefault?: number;
}

export const createSelectArg = (option: Option): core.ArgsRecord => {
  const { whereType, orderByType, takeDefault = 10, skipDefault = 0 } = option;
  return {
    where: arg({ type: whereType, nullable: true }),
    orderBy: arg({ type: orderByType, nullable: true }),
    take: intArg({ default: takeDefault }),
    skip: intArg({ default: skipDefault })
  };
};
