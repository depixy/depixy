import { BooleanFilter, DateTimeFilter, StringFilter } from "@prisma/client";
import { inputObjectType } from "@nexus/schema";

export interface DataTimeWhereArg extends DateTimeFilter {}
export interface StringWhereArg extends StringFilter {}
export interface BooleanWhereArg extends BooleanFilter {}

export const dateTimeWhere = inputObjectType({
  name: "DateTimeWhere",
  definition(t) {
    t.dateTime("equals", { nullable: true });
    t.dateTime("not", { nullable: true });
    t.list.dateTime("in", { nullable: true });
    t.list.dateTime("notIn", { nullable: true });
    t.dateTime("lt", { nullable: true });
    t.dateTime("lte", { nullable: true });
    t.dateTime("gt", { nullable: true });
    t.dateTime("gte", { nullable: true });
  }
});

export const stringWhere = inputObjectType({
  name: "StringWhere",
  definition(t) {
    t.string("equals", { nullable: true });
    t.string("not", { nullable: true });
    t.list.string("in", { nullable: true });
    t.list.string("notIn", { nullable: true });
    t.string("lt", { nullable: true });
    t.string("lte", { nullable: true });
    t.string("gt", { nullable: true });
    t.string("gte", { nullable: true });
    t.string("contains", { nullable: true });
    t.string("startsWith", { nullable: true });
    t.string("endsWith", { nullable: true });
  }
});

export const booleanWhere = inputObjectType({
  name: "BooleanWhere",
  definition(t) {
    t.boolean("equals", { nullable: true });
    t.boolean("not", { nullable: true });
  }
});
