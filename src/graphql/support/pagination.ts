import { objectType } from "@nexus/schema";

export const pagination = objectType({
  name: "Pagination",
  definition(t) {
    t.int("take");
    t.int("skip");
    t.int("total");
  }
});

export interface Pagination {
  take: number;
  skip: number;
  total: number;
}
