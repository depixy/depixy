import { objectType } from "@nexus/schema";

export const batchResult = objectType({
  name: "BatchResult",
  definition(t) {
    t.int("count");
  }
});

export interface BatchResult {
  count: number;
}
