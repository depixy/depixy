import { enumType } from "@nexus/schema";

export type OrderByArg = "asc" | "desc";
export const orderBy = enumType({
  name: "OrderBy",
  members: {
    asc: "asc",
    desc: "desc"
  }
});
