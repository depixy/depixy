import { objectType, queryField } from "@nexus/schema";
import { GalleryCategory } from "@prisma/client";

import { createSelectArg, Pagination, SelectArg } from "../../support";
import { AppContext } from "../../type";
import { GalleryCategoryOrderByArg, GalleryCategoryWhereArg } from "../model";

type Args = SelectArg<GalleryCategoryWhereArg, GalleryCategoryOrderByArg>;

const galleryCategoriesType = objectType({
  name: "GalleryCategories",
  definition(t) {
    t.list.field("data", {
      type: "GalleryCategory",
      args: createSelectArg({
        whereType: "GalleryCategoryWhere",
        orderByType: "GalleryCategoryOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<GalleryCategory[]> => ctx.prisma.galleryCategory.findMany(args)
    });
    t.field("pagination", {
      type: "Pagination",
      args: createSelectArg({
        whereType: "GalleryCategoryWhere",
        orderByType: "GalleryCategoryOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<Pagination> => {
        const { where, take, skip } = args;
        const total = await ctx.prisma.galleryCategory.count({ where });
        return { take, skip, total };
      }
    });
  }
});

const galleryCategories = queryField("galleryCategories", {
  type: "GalleryCategories",
  args: createSelectArg({
    whereType: "GalleryCategoryWhere",
    orderByType: "GalleryCategoryOrderBy"
  }),
  resolve: async (root, args: Args, ctx: AppContext) => ({})
});

export const galleryCategoriesTypes = [
  galleryCategories,
  galleryCategoriesType
];
