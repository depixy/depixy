import { queryIdField } from "../../func";

const galleryCategory = queryIdField("galleryCategory", {
  type: "GalleryCategory",
  resolve: (id, ctx) => ctx.prisma.galleryCategory.findOne({ where: { id } })
});
export const galleryCategoryTypes = [galleryCategory];
