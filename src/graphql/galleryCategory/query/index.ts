import { galleryCategoryTypes } from "./galleryCategory";
import { galleryCategoriesTypes } from "./galleryCategories";

export const queries = [...galleryCategoriesTypes, ...galleryCategoryTypes];
