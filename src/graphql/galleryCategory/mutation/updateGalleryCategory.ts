import { idArg, mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  id: string;
  name?: string;
};

const updateGalleryCategory = mutationField("updateGalleryCategory", {
  type: "GalleryCategory",
  args: {
    id: idArg(),
    name: stringArg({ nullable: true })
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const { id, name } = args;
    await ctx.hasPermission("galleryCategory::edit");
    return ctx.prisma.galleryCategory.update({
      data: { name },
      where: { id }
    });
  }
});

export const updateGalleryCategoryTypes = [updateGalleryCategory];
