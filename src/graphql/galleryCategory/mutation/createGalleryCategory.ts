import { mutationField, stringArg } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {
  name: string;
};
const createGalleryCategory = mutationField("createGalleryCategory", {
  type: "GalleryCategory",
  args: {
    name: stringArg()
  },
  resolve: async (_, args: Args, ctx: AppContext) => {
    const { name } = args;
    await ctx.hasPermission("galleryCategory::create");
    return ctx.prisma.galleryCategory.create({
      data: {
        name
      }
    });
  }
});

export const createGalleryCategoryTypes = [createGalleryCategory];
