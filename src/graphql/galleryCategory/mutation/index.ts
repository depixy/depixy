import { createGalleryCategoryTypes } from "./createGalleryCategory";
import { removeGalleryCategoriesTypes } from "./removeGalleryCategories";
import { removeGalleryCategoryTypes } from "./removeGalleryCategory";
import { updateGalleryCategoryTypes } from "./updateGalleryCategory";

export const mutations = [
  createGalleryCategoryTypes,
  removeGalleryCategoriesTypes,
  removeGalleryCategoryTypes,
  updateGalleryCategoryTypes
];
