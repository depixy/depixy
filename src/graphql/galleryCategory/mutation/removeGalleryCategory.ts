import { deleteById } from "../../func";
import { AppContext } from "../../type";

const removeGalleryCategory = deleteById({
  fieldName: "removeGalleryCategory",
  type: "GalleryCategory",
  resolve: async (id, ctx: AppContext) => {
    await ctx.hasPermission("galleryCategory::remove");
    return ctx.prisma.galleryCategory.delete({
      where: { id }
    });
  }
});

export const removeGalleryCategoryTypes = [removeGalleryCategory];
