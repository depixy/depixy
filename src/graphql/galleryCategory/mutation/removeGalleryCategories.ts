import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeGalleryCategories = deleteByIds({
  fieldName: "removeGalleryCategories",
  resolve: async (ids, ctx: AppContext) => {
    await ctx.hasPermission("galleryCategory::remove");
    return ctx.prisma.galleryCategory.deleteMany({
      where: {
        id: { in: ids }
      }
    });
  }
});

export const removeGalleryCategoriesTypes = [removeGalleryCategories];
