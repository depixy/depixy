import { models } from "./model";
import { queries } from "./query";
import { mutations } from "./mutation";

export const GalleryCategoryTypes = [...models, ...queries, ...mutations];

export { GalleryCategoryWhereArg, GalleryCategoryOrderByArg } from "./model";
