import { inputObjectType, objectType } from "@nexus/schema";

import { DataTimeWhereArg, OrderByArg, StringWhereArg } from "../support";

const galleryCategory = objectType({
  name: "GalleryCategory",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.boolean("default");
    t.dateTime("createdAt");
  }
});

const galleryCategoryOrderBy = inputObjectType({
  name: "GalleryCategoryOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const galleryCategoryWhere = inputObjectType({
  name: "GalleryCategoryWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.boolean("default", { nullable: false });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.list.field("AND", { type: "GalleryCategoryWhere", nullable: true });
    t.list.field("OR", { type: "GalleryCategoryWhere", nullable: true });
    t.list.field("NOT", { type: "GalleryCategoryWhere", nullable: true });
  }
});

export const models = [
  galleryCategory,
  galleryCategoryOrderBy,
  galleryCategoryWhere
];

export interface GalleryCategoryOrderByArg {
  name?: OrderByArg;
  createdAt?: OrderByArg;
}

export interface GalleryCategoryWhereArg {
  name?: StringWhereArg;
  default?: boolean;
  createdAt?: DataTimeWhereArg;
  AND?: GalleryCategoryWhereArg[];
  OR?: GalleryCategoryWhereArg[];
  NOT?: GalleryCategoryWhereArg[];
}
