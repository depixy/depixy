import { core } from "@nexus/schema";

export type Mutation = core.NexusExtendTypeDef<"Mutation">;
export type Query = core.NexusExtendTypeDef<"Query">;
export type ObjectType<T extends string> = core.NexusObjectTypeDef<T>;
export { AppContext } from "../middleware";
