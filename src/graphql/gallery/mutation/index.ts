import { createGalleryTypes } from "./createGallery";
import { removeGalleriesTypes } from "./removeGalleries";
import { removeGalleryTypes } from "./removeGallery";
import { updateGalleryTypes } from "./updateGallery";

export const mutations = [
  createGalleryTypes,
  removeGalleryTypes,
  removeGalleriesTypes,
  updateGalleryTypes
];
