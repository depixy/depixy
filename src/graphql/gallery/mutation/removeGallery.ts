import { deleteById } from "../../func";
import { AppContext } from "../../type";

const removeGallery = deleteById({
  fieldName: "removeGallery",
  type: "Gallery",
  resolve: async (id, ctx: AppContext) => {
    await ctx.hasPermission("gallery::remove");
    return ctx.prisma.gallery.delete({
      where: { id }
    });
  }
});

export const removeGalleryTypes = [removeGallery];
