import { arg, mutationField, stringArg } from "@nexus/schema";
import { Image } from "@prisma/client";
import { FileUpload } from "graphql-upload";
import { UserInputError } from "apollo-server-koa";

import { AppContext } from "../../type";

const getGalleryCategoryId = async (
  ctx: AppContext,
  categoryId?: string
): Promise<string | null> => {
  if (categoryId) {
    return categoryId;
  }
  const categories = await ctx.prisma.galleryCategory.findMany({
    where: { default: true }
  });
  return categories.length === 1 ? categories[0].id : null;
};

const saveTempFiles = async (
  ctx: AppContext,
  imageEntities: Image[],
  images: Promise<FileUpload>[]
): Promise<void> => {
  const uploads = await Promise.all(images);
  await Promise.all(
    uploads.map((upload, i) => {
      const { origin } = ctx.image.createKeys(imageEntities[i].id);
      return ctx.file.createTemp(ctx, upload.createReadStream(), origin);
    })
  );
};

const generateThumbnails = async (
  ctx: AppContext,
  images: Image[]
): Promise<void> => {
  const promises = images.map(image =>
    ctx.image.generateThumbnailFromOrigin(ctx, image.id)
  );
  await Promise.all(promises);
};

type Args = {
  name: string;
  description: string;
  categoryId?: string;
  source?: string;
  tagIds?: string[];
  images: Promise<FileUpload>[];
};

const createGallery = mutationField("createGallery", {
  type: "Gallery",
  args: {
    name: stringArg(),
    description: stringArg(),
    categoryId: stringArg({ nullable: true }),
    source: stringArg({ nullable: true }),
    images: arg({ type: "Upload", list: true }),
    tagIds: stringArg({ required: false, list: true })
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const { name, description, categoryId, source, images, tagIds } = args;
    await ctx.hasPermission("gallery::create");
    if (images.length <= 0) {
      throw new UserInputError("It must has at least 1 image.");
    }

    const cid = await getGalleryCategoryId(ctx, categoryId);
    if (!cid) {
      throw new UserInputError("Cannot find category.");
    }

    let gallery = null;
    const user = await ctx.getUserOrThrow();
    try {
      gallery = await ctx.prisma.gallery.create({
        include: {
          Image: true
        },
        data: {
          name,
          description,
          source,
          User: {
            connect: {
              id: user.id
            }
          },
          GalleryCategory: {
            connect: {
              id: cid
            }
          },
          Tag: {
            connect: tagIds?.map(id => ({ id }))
          },
          Image: {
            create: images.map((_, i) => ({ position: i }))
          }
        }
      });
      await saveTempFiles(ctx, gallery.Image, images);
      await generateThumbnails(ctx, gallery.Image);
      ctx.state.uploadImages.push(...gallery.Image.map(image => image.id));
      return gallery;
    } catch (e) {
      ctx.logger.error(`Error: ${e.message}`);
      if (gallery !== null) {
        ctx.prisma.gallery.delete({ where: { id: gallery.id } });
      }
      throw new UserInputError(e.message);
    }
  }
});

export const createGalleryTypes = [createGallery];
