import { idArg, mutationField, stringArg } from "@nexus/schema";
import { UserInputError } from "apollo-server-koa";

import { AppContext } from "../../type";

type Args = {
  id: string;
  name?: string;
  description?: string;
  categoryId?: string;
  tagIds?: string[];
};

const updateGallery = mutationField("updateGallery", {
  type: "Gallery",
  args: {
    id: idArg(),
    name: stringArg({ nullable: true }),
    description: stringArg({ nullable: true }),
    categoryId: stringArg({ nullable: true }),
    tagIds: stringArg({ required: false, list: true })
  },
  resolve: async (root, args: Args, ctx: AppContext) => {
    const { id, name, description, categoryId, tagIds } = args;
    const gallery = await ctx.prisma.gallery.findOne({
      where: { id },
      include: { User: true }
    });
    if (!gallery) {
      throw new UserInputError(`Invalid id: ${id}`);
    }
    await ctx.hasPermission("gallery::edit", { userId: gallery.User.id });
    const category = categoryId ? { connect: { id: categoryId } } : undefined;
    return ctx.prisma.gallery.update({
      data: {
        name,
        description,
        GalleryCategory: category,
        Tag: {
          connect: tagIds?.map(id => ({ id }))
        }
      },
      where: { id }
    });
  }
});

export const updateGalleryTypes = [updateGallery];
