import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeGalleries = deleteByIds({
  fieldName: "removeGalleries",
  resolve: async (ids, ctx: AppContext) => {
    await ctx.hasPermission("gallery::remove");
    return ctx.prisma.gallery.deleteMany({
      where: {
        id: { in: ids }
      }
    });
  }
});

export const removeGalleriesTypes = [removeGalleries];
