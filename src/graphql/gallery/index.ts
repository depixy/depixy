import { models } from "./model";
import { queries } from "./query";
import { mutations } from "./mutation";

export const GalleryTypes = [...models, ...queries, ...mutations];

export { GalleryWhereArg, GalleryOrderByArg } from "./model";
