import { galleryTypes } from "./gallery";
import { galleriesTypes } from "./galleries";

export const queries = [...galleriesTypes, ...galleryTypes];
