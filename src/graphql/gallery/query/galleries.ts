import { objectType, queryField } from "@nexus/schema";
import { Gallery } from "@prisma/client";

import { createSelectArg, Pagination, SelectArg } from "../../support";
import { AppContext } from "../../type";
import { GalleryOrderByArg, GalleryWhereArg } from "../model";

type Args = SelectArg<GalleryWhereArg, GalleryOrderByArg>;

const galleriesType = objectType({
  name: "Galleries",
  definition(t) {
    t.list.field("data", {
      type: "Gallery",
      args: createSelectArg({
        whereType: "GalleryWhere",
        orderByType: "GalleryOrderBy"
      }),
      resolve: async (root, args: Args, ctx: AppContext): Promise<Gallery[]> =>
        ctx.prisma.gallery.findMany(args)
    });
    t.field("pagination", {
      type: "Pagination",
      args: createSelectArg({
        whereType: "GalleryWhere",
        orderByType: "GalleryOrderBy"
      }),
      resolve: async (
        root,
        args: Args,
        ctx: AppContext
      ): Promise<Pagination> => {
        const { where, take, skip } = args;
        const total = await ctx.prisma.gallery.count({ where });
        return { take, skip, total };
      }
    });
  }
});

const galleries = queryField("galleries", {
  type: "Galleries",
  args: createSelectArg({
    whereType: "GalleryWhere",
    orderByType: "GalleryOrderBy"
  }),
  resolve: async (root, args: Args, ctx: AppContext) => ({})
});

export const galleriesTypes = [galleries, galleriesType];
