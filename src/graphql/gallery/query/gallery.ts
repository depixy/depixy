import { queryIdField } from "../../func";

const gallery = queryIdField("gallery", {
  type: "Gallery",
  resolve: (id, ctx) => ctx.prisma.gallery.findOne({ where: { id } })
});

export const galleryTypes = [gallery];
