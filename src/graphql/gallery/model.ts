import { inputObjectType, objectType } from "@nexus/schema";
import { Gallery } from "@prisma/client";

import { DataTimeWhereArg, OrderByArg, StringWhereArg } from "../support";
import { AppContext } from "../type";
import { UserWhereArg } from "../user";
import { TagFilterArg } from "../tag";
import { GalleryCategoryWhereArg } from "../galleryCategory";

const gallery = objectType({
  name: "Gallery",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.string("description");
    t.string("source", { nullable: true });
    t.dateTime("createdAt");
    t.field("author", {
      type: "User",
      resolve: (root: Gallery, _, ctx: AppContext) =>
        ctx.prisma.user.findOne({ where: { id: root.authorId } })
    });
    t.field("category", {
      type: "GalleryCategory",
      resolve: (root: Gallery, _, ctx: AppContext) =>
        ctx.prisma.galleryCategory.findOne({ where: { id: root.categoryId } })
    });
    t.list.field("tags", {
      type: "Tag",
      resolve: (root: Gallery, _, ctx: AppContext) =>
        ctx.prisma.tag.findMany({
          where: {
            Gallery: {
              some: {
                id: root.id
              }
            }
          }
        })
    });
    t.list.field("images", {
      type: "Image",
      resolve: (root: Gallery, _, ctx: AppContext) =>
        ctx.prisma.image.findMany({
          where: { galleryId: root.id },
          orderBy: { position: "asc" }
        })
    });
  }
});

const galleryOrderBy = inputObjectType({
  name: "GalleryOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const galleryWhere = inputObjectType({
  name: "GalleryWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.field("User", { type: "UserWhere", nullable: true });
    t.field("Tag", { type: "TagFilter", nullable: true });
    t.field("GalleryCategory", {
      type: "GalleryCategoryWhere",
      nullable: true
    });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.list.field("AND", { type: "GalleryWhere", nullable: true });
    t.list.field("OR", { type: "GalleryWhere", nullable: true });
    t.list.field("NOT", { type: "GalleryWhere", nullable: true });
  }
});

export const models = [gallery, galleryOrderBy, galleryWhere];

export interface GalleryOrderByArg {
  name?: OrderByArg;
  createdAt?: OrderByArg;
}

export interface GalleryWhereArg {
  name?: StringWhereArg;
  createdAt?: DataTimeWhereArg;
  User?: UserWhereArg;
  Tag?: TagFilterArg;
  GalleryCategory?: GalleryCategoryWhereArg;
  AND?: GalleryWhereArg[];
  OR?: GalleryWhereArg[];
  NOT?: GalleryWhereArg[];
}
