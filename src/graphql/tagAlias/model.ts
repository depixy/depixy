import { inputObjectType, objectType } from "@nexus/schema";
import { TagAlias } from "@prisma/client";

import { OrderByArg, StringWhereArg } from "../support";
import { AppContext } from "../type";

const tagAlias = objectType({
  name: "TagAlias",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.string("name");
    t.field("tag", {
      type: "Tag",
      resolve: (root: TagAlias, _, ctx: AppContext) =>
        ctx.prisma.tag.findOne({ where: { id: root.tagId } })
    });
  }
});

const tagAliasOrderBy = inputObjectType({
  name: "TagAliasOrderBy",
  definition(t) {
    t.field("name", { type: "OrderBy", nullable: true });
  }
});

const tagAliasWhere = inputObjectType({
  name: "TagAliasWhere",
  definition(t) {
    t.field("name", { type: "StringWhere", nullable: true });
    t.list.field("AND", { type: "TagAliasWhere", nullable: true });
    t.list.field("OR", { type: "TagAliasWhere", nullable: true });
    t.list.field("NOT", { type: "TagAliasWhere", nullable: true });
  }
});

const tagAliasFilter = inputObjectType({
  name: "TagAliasFilter",
  definition(t) {
    t.field("every", { type: "TagAliasWhere", nullable: true });
    t.field("some", { type: "TagAliasWhere", nullable: true });
    t.field("none", { type: "TagAliasWhere", nullable: true });
  }
});

export const models = [
  tagAlias,
  tagAliasOrderBy,
  tagAliasWhere,
  tagAliasFilter
];

export interface TagAliasOrderByArg {
  name?: OrderByArg;
}

export interface TagAliasWhereArg {
  name?: StringWhereArg;
  AND?: TagAliasWhereArg[];
  OR?: TagAliasWhereArg[];
  NOT?: TagAliasWhereArg[];
}

export interface TagAliasFilterArg {
  every?: TagAliasWhereArg;
  some?: TagAliasWhereArg;
  none?: TagAliasWhereArg;
}
