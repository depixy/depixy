import { models } from "./model";

export const TagAliasTypes = [...models];

export {
  TagAliasWhereArg,
  TagAliasOrderByArg,
  TagAliasFilterArg
} from "./model";
