import { models } from "./model";
import { mutations } from "./mutation";

export const UserTokenTypes = [...models, ...mutations];

export { UserTokenWhereArg, UserTokenOrderByArg } from "./model";
