import { inputObjectType, objectType } from "@nexus/schema";
import { UserToken } from "@prisma/client";

import { AppContext } from "../type";
import { DataTimeWhereArg, OrderByArg, StringWhereArg } from "../support";
import { UserWhereArg } from "../user";

const userToken = objectType({
  name: "UserToken",
  definition(t) {
    t.id("id", { description: "UUID for a resource" });
    t.dateTime("createdAt");
    t.field("user", {
      type: "User",
      resolve: (root: UserToken, _, ctx: AppContext) =>
        ctx.prisma.user.findOne({ where: { id: root.userId } })
    });
  }
});

const userTokenOrderBy = inputObjectType({
  name: "UserTokenOrderBy",
  definition(t) {
    t.field("createdAt", { type: "OrderBy", nullable: true });
  }
});

const userTokenWhere = inputObjectType({
  name: "UserTokenWhere",
  definition(t) {
    t.field("userId", { type: "StringWhere", nullable: true });
    t.field("createdAt", { type: "DateTimeWhere", nullable: true });
    t.field("User", { type: "UserWhere", nullable: true });
    t.list.field("AND", { type: "UserTokenWhere", nullable: true });
    t.list.field("OR", { type: "UserTokenWhere", nullable: true });
    t.list.field("NOT", { type: "UserTokenWhere", nullable: true });
  }
});

export const models = [userToken, userTokenOrderBy, userTokenWhere];

export interface UserTokenOrderByArg {
  createdAt?: OrderByArg;
}

export interface UserTokenWhereArg {
  userId?: StringWhereArg;
  createdAt?: DataTimeWhereArg;
  User?: UserWhereArg;
  AND?: UserTokenWhereArg[];
  OR?: UserTokenWhereArg[];
  NOT?: UserTokenWhereArg[];
}
