import { mutationField } from "@nexus/schema";

import { AppContext } from "../../type";

type Args = {};
const createUserToken = mutationField("createUserToken", {
  type: "UserToken",
  args: {},
  resolve: async (root, args: Args, ctx: AppContext) => {
    await ctx.hasPermission("userToken::create");
    const user = await ctx.getUserOrThrow();
    return ctx.prisma.userToken.create({
      data: {
        User: {
          connect: { id: user.id }
        }
      }
    });
  }
});

const login = mutationField("login", {
  type: "Boolean",
  args: {},
  resolve: async (root, args: Args, ctx: AppContext) => {
    await ctx.hasPermission("userToken::create");
    const user = await ctx.getUserOrThrow();
    const token = await ctx.prisma.userToken.create({
      data: {
        User: {
          connect: { id: user.id }
        }
      }
    });
    ctx.setSession(token.id);
    return true;
  }
});

export const createUserTokenTypes = [createUserToken, login];
