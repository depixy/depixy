import { createUserTokenTypes } from "./createUserToken";
import { removeUserTokensTypes } from "./removeUserTokens";
import { removeUserTokenTypes } from "./removeUserToken";

export const mutations = [
  createUserTokenTypes,
  removeUserTokensTypes,
  removeUserTokenTypes
];
