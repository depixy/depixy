import { deleteByIds } from "../../func";
import { AppContext } from "../../type";

const removeUserTokens = deleteByIds({
  fieldName: "removeUserTokens",
  resolve: async (ids, ctx: AppContext) => {
    const user = await ctx.getUserOrThrow();
    return ctx.prisma.userToken.deleteMany({
      where: { id: { in: ids }, userId: { equals: user.id } }
    });
  }
});

export const removeUserTokensTypes = [removeUserTokens];
