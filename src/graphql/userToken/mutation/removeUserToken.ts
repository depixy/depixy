import { mutationField } from "@nexus/schema";

import { deleteById } from "../../func";
import { AppContext } from "../../type";

type Args = {};

const removeUserToken = deleteById({
  fieldName: "removeUserToken",
  type: "UserToken",
  resolve: async (id, ctx: AppContext) => {
    const userToken = await ctx.prisma.userToken.findOne({
      where: { id },
      include: { User: true }
    });
    if (!userToken) {
      return null;
    }
    await ctx.hasPermission("userToken::remove", { userId: userToken.User.id });
    return ctx.prisma.user.delete({
      where: { id }
    });
  }
});

const logout = mutationField("logout", {
  type: "Boolean",
  args: {},
  resolve: async (root, args: Args, ctx: AppContext) => {
    const user = await ctx.getUserOrThrow();
    const { userToken: id } = ctx.state;
    if (!id) {
      return false;
    }
    await ctx.hasPermission("userToken::remove", { userId: user.id });
    await ctx.prisma.userToken.delete({
      where: { id }
    });
    ctx.removeSession();
    return true;
  }
});

export const removeUserTokenTypes = [removeUserToken, logout];
