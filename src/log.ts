import {
  createLogger as createWinstonLogger,
  format,
  Logger,
  transports
} from "winston";

export const createLogger = (isProduction: boolean): Logger =>
  createWinstonLogger({
    level: isProduction ? "info" : "debug",
    transports: [new transports.Console()],
    format: format.combine(
      format.timestamp(),
      format.printf(info => {
        const { timestamp, level, message } = info;
        return `${timestamp} [${level}] ${message}`;
      })
    )
  });
