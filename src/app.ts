import Koa from "koa";
import { ApolloServer } from "apollo-server-koa";

import { schema } from "./graphql";
import {
  authMiddleware,
  databaseMiddleware,
  fileMiddleware,
  imageMiddleware,
  logMiddleware,
  permissionMiddleware,
  s3Middleware,
  sessionMiddleware,
  uploadMiddleware
} from "./middleware";
import {
  getEnvBoolean,
  getEnvInt,
  getEnvIntOrUndefined,
  getEnvString,
  getEnvStringOrUndefined
} from "./func";
import { createLogger } from "./log";
import { router } from "./router";

interface EnvVars {
  tempDir: string;
  nodeEnv: string | undefined;
  endPoint: string;
  useSSL: boolean;
  accessKey: string;
  secretKey: string;
  bucket: string;
  s3port: number | undefined;
  port: number;
  cookieSecret: string;
  cookieSecure: boolean;
}
const getEnvVars = (): EnvVars => ({
  tempDir: getEnvString("DEPIXY_TEMP_DIR"),
  nodeEnv: getEnvStringOrUndefined("NODE_ENV"),
  endPoint: getEnvString("DEPIXY_S3_END_POINT"),
  useSSL: getEnvBoolean("DEPIXY_S3_USE_SSL", true),
  accessKey: getEnvString("DEPIXY_S3_ACCESS_KEY"),
  secretKey: getEnvString("DEPIXY_S3_SECRET_KEY"),
  bucket: getEnvString("DEPIXY_S3_BUCKET"),
  s3port: getEnvIntOrUndefined("DEPIXY_S3_PORT"),
  port: getEnvInt("DEPIXY_PORT"),
  cookieSecret: getEnvString("DEPIXY_COOKIE_SECRET"),
  cookieSecure: getEnvBoolean("DEPIXY_COOKIE_SECURE", false)
});

export const startApp = async (): Promise<void> => {
  const {
    tempDir,
    nodeEnv,
    endPoint,
    useSSL,
    accessKey,
    secretKey,
    bucket,
    s3port,
    port,
    cookieSecret,
    // TODO
    cookieSecure
  } = getEnvVars();
  const app = new Koa();
  app.keys = [cookieSecret];
  const server = new ApolloServer({
    schema,
    context: ctx => ctx.ctx,
    uploads: { maxFileSize: 10000000 }
  });
  const logger = createLogger(nodeEnv === "production");
  app.use(sessionMiddleware());
  app.use(logMiddleware(logger));
  app.use(fileMiddleware(tempDir));
  app.use(imageMiddleware());
  app.use(databaseMiddleware());
  app.use(
    await s3Middleware({
      endPoint,
      useSSL,
      accessKey,
      secretKey,
      bucket,
      port: s3port ? s3port : useSSL ? 443 : 80
    })
  );
  app.use(authMiddleware());
  app.use(permissionMiddleware());
  app.use(uploadMiddleware());
  app.use(router.routes()).use(router.allowedMethods());
  server.applyMiddleware({ app });
  logger.info("Depixy is starting.");
  app.listen(port);
};
