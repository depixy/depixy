import Router from "@koa/router";

import { AppContext, AppState } from "./middleware/type";

export const router = new Router<AppState, AppContext>();

router.post("/login", async (ctx, next) => {
  await next();
});
