import { Client, ClientOptions } from "minio";

import { AppMiddleware } from "./type";

export interface S3Context {
  s3: Client;
  bucket: string;
}

export const s3Middleware = async (
  options: ClientOptions & { bucket: string }
): Promise<AppMiddleware> => {
  const { bucket, ...client } = options;
  const s3 = new Client(client);
  const bucketExists = await s3.bucketExists(bucket);
  if (!bucketExists) {
    throw new Error(`Bucket ${bucket} does not exists.`);
  }
  return async (ctx, next) => {
    ctx.s3 = s3;
    ctx.bucket = bucket;
    await next();
  };
};
