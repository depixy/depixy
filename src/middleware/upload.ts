import { AppContext, AppMiddleware } from "./type";
import { lookup } from "mime-types";

const uploadImage = async (ctx: AppContext, key: string[]): Promise<void> => {
  const objectName = key.join("/");
  const filePath = ctx.file.getPath(key);
  try {
    await ctx.s3.fPutObject(ctx.bucket, objectName, filePath, {
      "Content-Type": lookup(ctx.file.getPath(key))
    });
  } catch (error) {
    ctx.logger.error("Fail to upload image", { objectName, error });
  }
};

const uploadImages = async (ctx: AppContext): Promise<void> => {
  const promises = ctx.state.uploadImages.flatMap(imageId => {
    const { origin, thumbnail } = ctx.image.createKeys(imageId);
    return [uploadImage(ctx, origin), uploadImage(ctx, thumbnail)];
  });
  await Promise.all(promises);
};

const removeImages = async (ctx: AppContext): Promise<void> => {
  const keys = ctx.state.removeImages.flatMap(imageId => {
    const { origin, thumbnail } = ctx.image.createKeys(imageId);
    return [origin.join("/"), thumbnail.join("/")];
  });
  await ctx.s3.removeObjects(ctx.bucket, keys);
};

export interface UploadState {
  uploadImages: string[];
  removeImages: string[];
}

export const uploadMiddleware = (): AppMiddleware => async (ctx, next) => {
  ctx.state.uploadImages = [];
  ctx.state.removeImages = [];
  await next();
  await Promise.all([uploadImages(ctx), removeImages(ctx)]);
};
