import { Logger } from "winston";

import { AppMiddleware } from "./type";

export interface LogContext {
  logger: Logger;
}

export const logMiddleware = (logger: Logger): AppMiddleware => async (
  ctx,
  next
) => {
  ctx.logger = logger;
  await next();
};
