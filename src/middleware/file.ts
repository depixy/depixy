import { createReadStream, createWriteStream, promises, ReadStream } from "fs";
import { Readable } from "stream";
import path from "path";

import { AppContext, AppMiddleware } from "./type";
import { toPromise } from "./func";

type GetPathFn = (keys: string[]) => string;

const createTemp = (getPath: GetPathFn) => async (
  ctx: AppContext,
  rs: Readable,
  keys: string[]
): Promise<void> => {
  const filePath = getPath(keys);
  await promises.mkdir(path.dirname(filePath), { recursive: true });
  const ws = createWriteStream(filePath);
  ctx.state.tempFiles.push(filePath);
  await toPromise(rs, ws);
};

const readTemp = (getPath: GetPathFn) => (
  ctx: AppContext,
  keys: string[]
): ReadStream => {
  const filePath = getPath(keys);
  return createReadStream(filePath);
};

const clearTemp = async (ctx: AppContext): Promise<void> => {
  const clearTempPromises = ctx.state.tempFiles.map(async filePath => {
    try {
      await promises.unlink(filePath);
      const parentDir = path.dirname(filePath);
      const files = await promises.readdir(parentDir);
      if (files.length <= 0) {
        await promises.rmdir(parentDir);
      }
    } catch (error) {
      if (error.code === "ENOENT") {
        ctx.logger.debug(`${filePath} does not exists.`);
      } else {
        ctx.logger.warn(`Error occurred while trying to remove ${filePath}.`, {
          error
        });
      }
    }
  });
  await Promise.all(clearTempPromises);
};

export interface FileContext {
  file: {
    getPath: GetPathFn;
    createTemp: ReturnType<typeof createTemp>;
    readTemp: ReturnType<typeof readTemp>;
  };
}

export interface FileState {
  tempFiles: string[];
}

export const fileMiddleware = (tempDir: string): AppMiddleware => {
  const getPath: GetPathFn = keys => path.join(tempDir, ...keys);

  const file = {
    getPath,
    createTemp: createTemp(getPath),
    readTemp: readTemp(getPath)
  };
  return async (ctx, next) => {
    ctx.state.tempFiles = [];
    ctx.file = file;
    try {
      await next();
    } finally {
      await clearTemp(ctx);
    }
  };
};
