import { User } from "@prisma/client";
import bcrypt from "bcrypt";
import _ from "lodash";

import { AppContext, AppMiddleware } from "./type";

const handleBearerAuth = async (
  ctx: AppContext,
  token: string
): Promise<void> => {
  const userToken = await ctx.prisma.userToken.findOne({
    where: { id: token },
    include: { User: true }
  });
  const user = userToken?.User;
  if (!user) {
    ctx.removeSession();
    ctx.state.auth = false;
    ctx.throw(403, "Invalid authorization token");
  }
  ctx.state.auth = true;
  ctx.state.user = user;
  ctx.state.userToken = token;
};

const handleBasicAuth = async (
  ctx: AppContext,
  token: string
): Promise<void> => {
  const decoded = Buffer.from(token, "base64").toString();
  const result = /(.+):(.+)/.exec(decoded);
  if (!result) {
    ctx.state.auth = false;
    ctx.throw(400, "Invalid basic authentication");
  }
  const [, name, password] = result;
  const user = await ctx.prisma.user.findOne({ where: { name } });
  if (!user) {
    ctx.state.auth = false;
    ctx.throw(403, "Invalid name or password");
  }
  const validPassword = await ctx.comparePassword(password, user.password);
  if (!validPassword) {
    ctx.state.auth = false;
    ctx.throw(403, "Invalid name or password");
  }
  ctx.state.auth = true;
  ctx.state.user = user;
};

const hashPassword = (password: string): Promise<string> =>
  bcrypt.hash(password, 12);

const comparePassword = (
  password: string,
  passwordHash: string
): Promise<boolean> => bcrypt.compare(password, passwordHash);

export interface AuthContext {
  hashPassword: (password: string) => Promise<string>;
  comparePassword: (password: string, passwordHash: string) => Promise<boolean>;
  getUser(): Promise<User | undefined>;
  getUserOrThrow(): Promise<User>;
}

export interface AuthState {
  auth?: boolean;
  user?: User;
  userToken?: string;
}

const parseAuthorizationHeader = (
  ctx: AppContext,
  auth: string
): [string, string] => {
  const parts = /^(\S+)\s+(\S+)$/g.exec(auth);
  if (!parts) {
    ctx.state.auth = false;
    ctx.throw(400, "Invalid authorization header");
  }
  return [parts[1], parts[2]];
};

const handleAuthorizationHeader = async (ctx: AppContext): Promise<void> => {
  const authorization = ctx.request.get("Authorization");
  if (!authorization || !_.isNil(ctx.state.auth)) {
    return;
  }
  const [type, token] = parseAuthorizationHeader(ctx, authorization);
  switch (type) {
    case "Bearer":
      await handleBearerAuth(ctx, token);
      break;
    case "Basic":
      await handleBasicAuth(ctx, token);
      break;
    default:
      ctx.state.auth = false;
      ctx.throw(400, "Invalid authorization type");
  }
};

const handleUserTokenCookies = async (ctx: AppContext): Promise<void> => {
  const token = ctx.getSession();
  if (!token || !_.isNil(ctx.state.auth)) {
    return;
  }
  await handleBearerAuth(ctx, token);
};

const getUser = (ctx: AppContext) => async (): Promise<User | undefined> => {
  if (_.isNil(ctx.state.auth)) {
    await handleAuthorizationHeader(ctx);
    await handleUserTokenCookies(ctx);
  }
  return ctx.state.user;
};

const getUserOrThrow = (ctx: AppContext) => async (): Promise<User> => {
  const user = await ctx.getUser();
  if (!user) {
    ctx.throw(403, "Unauthorized");
  }
  return user;
};

export const authMiddleware = (): AppMiddleware => async (ctx, next) => {
  ctx.hashPassword = hashPassword;
  ctx.comparePassword = comparePassword;
  ctx.getUser = getUser(ctx);
  ctx.getUserOrThrow = getUserOrThrow(ctx);
  await next();
};
