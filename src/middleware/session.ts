import { AppContext, AppMiddleware } from "./type";

export interface SessionContext {
  removeSession: () => void;
  getSession: () => string | undefined;
  setSession: (value: string) => void;
}

const removeSession = (ctx: AppContext) => (): void => {
  ctx.cookies.set("Authorization", { signed: true });
};

const getSession = (ctx: AppContext) => (): string | undefined =>
  ctx.cookies.get("Authorization", { signed: true });

const setSession = (ctx: AppContext) => (value: string): void => {
  ctx.cookies.set("Authorization", value, { signed: true });
};

export const sessionMiddleware = (): AppMiddleware => async (ctx, next) => {
  ctx.removeSession = removeSession(ctx);
  ctx.getSession = getSession(ctx);
  ctx.setSession = setSession(ctx);
  await next();
};
