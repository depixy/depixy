import { User } from "@prisma/client";

import { AppContext, AppMiddleware } from "./type";

const isAdmin = (user: User): boolean => user.admin;
const isUser = (user: User): boolean => !!user;
const isUserId = (user: User, userId?: string): boolean => user.id === userId;

/* eslint-disable complexity */
const hasPermission = (ctx: AppContext) => async (
  permission: string,
  data: Record<string, any> = {}
): Promise<void> => {
  const user = await ctx.getUserOrThrow();
  let allow = true;
  switch (permission) {
    case "gallery::create":
      allow = isUser(user);
      break;
    case "gallery::remove":
      allow = isAdmin(user);
      break;
    case "gallery::edit": {
      const { userId } = data;
      allow = isUserId(user, userId) || isAdmin(user);
      break;
    }
    case "galleryCategory::create":
    case "galleryCategory::remove":
    case "galleryCategory::edit":
      allow = isAdmin(user);
      break;
    case "tag::create":
    case "tag::remove":
    case "tag::edit":
      allow = isAdmin(user);
      break;
    case "tagCategory::create":
    case "tagCategory::remove":
    case "tagCategory::edit":
      allow = isAdmin(user);
      break;
    case "user::create":
    case "user::remove":
      allow = isAdmin(user);
      break;
    case "user::edit": {
      const { userId } = data;
      allow = isUserId(user, userId) || isAdmin(user);
      break;
    }
    case "userToken::create":
      allow = isUser(user);
      break;
    case "userToken::remove": {
      const { userId } = data;
      allow = isUserId(user, userId);
      break;
    }
    default:
      ctx.throw(403, `Unknown permission (${permission})`);
  }
  if (!allow) {
    ctx.throw(403, "Not enough permission");
  }
};
/* eslint-enable complexity */

export interface PermissionContext {
  hasPermission: (
    permission: string,
    data?: Record<string, any>
  ) => Promise<void>;
}

export const permissionMiddleware = (): AppMiddleware => async (ctx, next) => {
  ctx.hasPermission = hasPermission(ctx);
  await next();
};
