import { DefaultState, Middleware, ParameterizedContext } from "koa";

import { FileContext, FileState } from "./file";
import { UploadState } from "./upload";
import { DatabaseContext } from "./database";
import { AuthContext, AuthState } from "./auth";
import { ImageContext } from "./image";
import { LogContext } from "./log";
import { S3Context } from "./s3";
import { PermissionContext } from "./permission";
import { SessionContext } from "./session";

export interface AppState
  extends DefaultState,
    FileState,
    UploadState,
    AuthState {}

interface Context
  extends DatabaseContext,
    AuthContext,
    FileContext,
    ImageContext,
    LogContext,
    S3Context,
    PermissionContext,
    SessionContext {}

export interface AppContext extends ParameterizedContext<AppState, Context> {}

export interface AppMiddleware extends Middleware<AppState, AppContext> {}
