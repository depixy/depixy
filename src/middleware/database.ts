import { PrismaClient } from "@prisma/client";

import { AppMiddleware } from "./type";

export interface DatabaseContext {
  prisma: PrismaClient;
}

export const databaseMiddleware = (): AppMiddleware => {
  const prisma = new PrismaClient();
  return async (ctx, next) => {
    ctx.prisma = prisma;
    await next();
  };
};
