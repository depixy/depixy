import sharp from "sharp";

import { toPromise } from "./func";
import { AppContext, AppMiddleware } from "./type";

const createKeys = (
  id: string
): {
  origin: string[];
  thumbnail: string[];
} => ({
  origin: [id, "origin"],
  thumbnail: [id, "thumbnail"]
});

const generateThumbnail = async (
  ctx: AppContext,
  fromKey: string[],
  toKey: string[]
): Promise<void> => {
  const rs = ctx.file.readTemp(ctx, fromKey);
  const imageFilter = sharp()
    .resize(250, 250)
    .jpeg({ progressive: true, force: false })
    .png({ progressive: true, force: false });
  await toPromise(rs, imageFilter);
  await ctx.file.createTemp(ctx, imageFilter, toKey);
};

const generateThumbnailFromOrigin = async (
  ctx: AppContext,
  id: string
): Promise<void> => {
  const { origin, thumbnail } = createKeys(id);
  return generateThumbnail(ctx, origin, thumbnail);
};
export const image = {
  generateThumbnail,
  generateThumbnailFromOrigin,
  createKeys
};

export interface ImageContext {
  image: {
    generateThumbnail: typeof generateThumbnail;
    generateThumbnailFromOrigin: typeof generateThumbnailFromOrigin;
    createKeys: typeof createKeys;
  };
}

export const imageMiddleware = (): AppMiddleware => async (ctx, next) => {
  ctx.image = image;
  await next();
};
