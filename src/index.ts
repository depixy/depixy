import dotenv from "dotenv";
dotenv.config();

import { startApp } from "./app";

startApp();
